var _WM_APP_PROPERTIES = {
  "activeTheme" : "material",
  "dateFormat" : "",
  "defaultLanguage" : "en",
  "displayName" : "Git_Repo",
  "homePage" : "Main",
  "name" : "Git_Repo",
  "platformType" : "WEB",
  "supportedLanguages" : "en",
  "timeFormat" : "",
  "type" : "APPLICATION",
  "version" : "1.0"
};